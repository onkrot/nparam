﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace nparam
{
    class ModelGenerator
    {
        List<List<int>> mod = new List<List<int>>();
        List<int> tm = new List<int>();
        readonly double corelX;
        readonly double corelY;
        readonly double alp;
        int m;
        int n;
        public ModelGenerator(double _corelX, double _corelY, double _alp)
        {
            corelX = _corelX;
            corelY = _corelY;
            alp = _alp;  
        }
        public List<RegressModel> GenerateModels(List<Vector<double>> par, Vector<double> Y)
        {
            m = par.Count;
            n = Y.Count;

            // Выбираем только те параметры, для которых коэффициент корелляции с Y значим
            StudentT student = new StudentT(0, 1, n - 2);
            List<Vector<double>> good = new List<Vector<double>>
            {
                DenseVector.Create(770, 1)
            };
            for (int i = 0; i < m; i++)
            {
                double c = Math.Abs(Correlation.Pearson(par[i], Y));
                if (util.CheckCorrelation(c, n, m, student, alp) && c > corelY)
                    good.Add(par[i]);
            }
            m = good.Count;

            // Из них формируем группы сильно кореллированых между собой
            List<List<int>> models_col = new List<List<int>>();
            for (int i = 1; i < m; i++)
            {
                bool found = false;
                int f_i = 0;
                for (int j = 0; j < models_col.Count; j++)
                {
                    for (int k = 0; k < models_col[j].Count; k++)
                    {
                        if (Correlation.Pearson(good[i], good[models_col[j][k]]) > corelX)
                        {
                            found = true;
                            f_i = j;
                            goto exit;
                        }
                    }
                }
            exit:
                if (found)
                {
                    models_col[f_i].Add(i);
                }
                else
                {
                    models_col.Add(new List<int>());
                    models_col.Last().Add(i);
                }
            }

            // Преобразуем список групп к списку параметров моделей
            Helper(models_col, 0);
            var models = mod;

            // Формируем модели
            List<RegressModel> rm = new List<RegressModel>();
            foreach (var it in models)
            {
                List<Vector<double>> u = new List<Vector<double>>
                {
                    good[0]
                };
                for (int i = 0; i < it.Count; i++)
                {
                    u.Add(good[it[i]]);
                }
                Matrix<double> matrix = DenseMatrix.OfColumnVectors(u.ToArray());
                var rc = new RegressModel(matrix, Y, alp, it);
                if (true || rc.HasErrorNormalDistribution())
                {
                    rm.Add(rc);
                }
                
            }
            return rm;
        }

        // Рекурсивная функция формирования списков параметров
        void Helper(List<List<int>> m_c, int depth)
        {
            if (depth == m_c.Count)
            {
                mod.Add(tm.ToList());
                return;
            }
            for (int i = 0; i < m_c[depth].Count; i++)
            {
                tm.Add(m_c[depth][i]);
                Helper(m_c, depth + 1);
                tm.RemoveAt(tm.Count - 1);
            }
        }
    }
}
