﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nparam
{
    class RegressModel
    {
        private Vector<double> coef;
        private Matrix<double> X;
        private Vector<double> Y;
        private Vector<double> Y_v;
        private Matrix<double> XTXi;
        private List<int> num;
        private readonly int n;
        private readonly int m;
        private readonly double alp;
        public RegressModel(Matrix<double> _X, Vector<double> _Y, double _alp, List<int> _num)
        {
            X = _X.Clone();
            Y = _Y.Clone();
            m = X.ColumnCount;
            n = X.RowCount;
            alp = _alp;
            num = _num;

            Matrix<double> xtx = X.Transpose().Multiply(X);
            xtx = xtx.Inverse();
            Vector<double> xty = X.Transpose().Multiply(Y);
            coef = xtx.Multiply(xty);
            XTXi = X.Transpose().Multiply(X).Inverse();

            Y_v = DenseVector.Create(n, 0);
            for (int i = 0; i < n; i++)
            {
                Y_v[i] = F(X.Row(i));
            }

        }

        // Адекватность модели
        public bool IsModelAdequate()
        {
            FisherSnedecor fisher = new FisherSnedecor(m, n - m - 1);
            return F_equ() > fisher.InverseCumulativeDistribution(1 - alp);
        }

        // Имеет ли ошибка нормальное распределение
        public bool HasErrorNormalDistribution()
        {
            bool res = false;
            Vector<double> err_t = Error;
            //double avg = err_t.Average();
            double sigma = err_t.StandardDeviation() * 3;
            bool check = true;
            foreach (var i in err_t)
            {
                if (Math.Abs(i) > sigma) check = false;
            }
            res = check;
            res &= (util.OmegaSquStat(err_t) < 1.66);
            return res;
        }

        // Вычислить уравнение для данного набора параметров
        public double F(Vector<double> x)
        {
            return coef.DotProduct(x);
        }

        // Вычисление коэфициента детеминации за определённый период
        public double Determination(int start, int count)
        {
            int k = count;
            //Y_x - вычисленное значение, Y_d - данное
            Vector<double> Y_x = DenseVector.Create(count, 0);
            Vector<double> Y_d = DenseVector.Create(count, 0);
            for (int i = 0; i < count; i++)
            {
                Y_x[i] = F(X.Row(i + start));
                Y_d[i] = Y[i + start];
            }
            double ya = Y_d.Average();
            double us = 0, ds = 0;
            for (int i = 0; i < Y_d.Count; i++)
            {
                us += Math.Pow(Y_d[i] - Y_x[i], 2.0);
                ds += Math.Pow(Y_d[i] - ya, 2.0);
            }
            double R = 1 - (us / ds);
            //double RR = 1 - ((double)(k - 1) / (k - m)) * (1 - R);
            return R;
        }

        // Возвращает строковое представление уравнения
        public string GetEquation()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("y=");
            sb.Append(string.Format("{0:F3}", coef[0]));
            for (int i = 1; i < coef.Count; i++)
            {
                if(coef[i] > 0)
                    sb.Append(string.Format(" + {0:F3}x{1}", coef[i], num[i - 1] + 1));
                else
                    sb.Append(string.Format(" {0:F3}x{1}", coef[i], num[i - 1] + 1));
            }
            return sb.ToString();
        }

        // Ошибка модели
        Vector<double> Error => Y - X.Multiply(coef);

        // Коэфициент T-статистики для одного параметра
        double T_coef(int k)
        {
            Vector<double> err = Error;
            double sigma = err.DotProduct(err);
            sigma = sigma / (n - m - 1.0);
            double s_b = Math.Sqrt(XTXi[k, k] * sigma);
            return coef[k] / s_b;
        }

        // Коэфициент F-статистики для модели
        double F_equ()
        {
            double R = Determination(0, 770);
            double us = R / m;
            double ds = (1 - R) / (n - m - 1);
            return us / ds;
        }
        public Tuple<double, double> RebuilderDetermination()
        {
            List<RegressModel> rms = new List<RegressModel>();
            List<Vector<double>> nX = new List<Vector<double>>();
            Vector<double> det = DenseVector.Create(10, 0);
            List<double> y = new List<double>();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 77; j++)
                {
                    nX.Add(X.Row(i * 77 + j));
                    y.Add(Y[i * 77 + j]);
                }
                RegressModel rc = new RegressModel(DenseMatrix.OfRowVectors(nX.ToArray()), DenseVector.OfArray(y.ToArray()), alp, num);
                det[i] = rc.Determination(0, y.Count);
                rms.Add(rc);
            }
            double t_avg = det.Average();
            double t_st = det.StandardDeviation();
            return new Tuple<double, double>(t_avg, t_st);
        }
        public Tuple<double, double> RebuilderDelta()
        {
            List<RegressModel> rms = new List<RegressModel>();
            List<Vector<double>> nX = new List<Vector<double>>();
            List<double> y = new List<double>();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 77; j++)
                {
                    nX.Add(X.Row(i * 77 + j));
                    y.Add(Y[i * 77 + j]);
                }
                RegressModel rc = new RegressModel(DenseMatrix.OfRowVectors(nX.ToArray()), DenseVector.OfArray(y.ToArray()), alp, num);
                rms.Add(rc);
            }
            Vector<double> Z = DenseVector.Create(9, 0);
            for (int i = 0; i < 9; i++)
            {
                double z = 0;
                for (int j = 0; j < coef.Count; j++)
                {
                    z += Math.Abs(rms[i].coef[j] - rms[i + 1].coef[j] / rms[i].coef[j]); 
                }
                z /= coef.Count;
                Z[i] = z;
            }
            double z_avg = Z.Average();
            double z_st = Z.StandardDeviation();
            return new Tuple<double, double>(z_avg, z_st);
        }
        public Tuple<double, double> RebuilderDeltaYear()
        {
            List<RegressModel> rms = new List<RegressModel>();
            List<Vector<double>> nX = new List<Vector<double>>();
            List<double> y = new List<double>();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 77; j++)
                {
                    nX.Add(X.Row(i * 77 + j));
                    y.Add(Y[i * 77 + j]);
                }
                RegressModel rc = new RegressModel(DenseMatrix.OfRowVectors(nX.ToArray()), DenseVector.OfArray(y.ToArray()), alp, num);
                rms.Add(rc);
                nX.Clear();
                y.Clear();
            }
            Vector<double> Z = DenseVector.Create(9, 0);
            for (int i = 0; i < 9; i++)
            {
                double z = 0;
                for (int j = 0; j < coef.Count; j++)
                {
                    z += Math.Abs(rms[i].coef[j] - rms[i + 1].coef[j] / rms[i].coef[j]);
                }
                z /= coef.Count;
                Z[i] = z;
            }
            double z_avg = Z.Average();
            double z_st = Z.StandardDeviation();
            return new Tuple<double, double>(z_avg, z_st);
        }
    }
}
