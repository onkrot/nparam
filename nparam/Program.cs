﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace nparam
{
    class Program
    {         
        static int BestModelDet(List<RegressModel> rm)
        {
            int num = 0;
            RegressModel rs = rm[num];
            var t = rm[0].RebuilderDetermination();
            double best_var = t.Item2 / t.Item1;
            double best_avg = t.Item1;
            double best_st = t.Item2;
            for (int i = 1; i < rm.Count; i++)
            {
                t = rm[i].RebuilderDetermination();
                double t_avg = t.Item1;
                double t_st = t.Item2;
                //double t_var = t.Item2 / t.Item1;
                if (t_avg > best_avg && t_st < best_st)
                {
                    num = i;
                    best_avg = t_avg;
                    best_st = t_st;
                }
            }
            return num;
        }
        static int BestModelVar(List<RegressModel> rm)
        {
            int num = 0;
            RegressModel rs = rm[num];
            var t = rm[0].RebuilderDelta();
            double best_avg  = t.Item1;
            double best_st = t.Item2;
            for (int i = 1; i < rm.Count; i++)
            {
                t = rm[i].RebuilderDelta();
                double t_avg = t.Item1;
                double t_st = t.Item2;
                if (t_avg < best_avg && t_st < best_st)
                {
                    num = i;
                    best_avg = t_avg;
                    best_st = t_st;
                }
            }
            return num;
        }
        static int BestModelVarYear(List<RegressModel> rm)
        {
            int num = 0;
            RegressModel rs = rm[num];
            var t = rm[0].RebuilderDeltaYear();
            double best_avg = t.Item1;
            double best_st = t.Item2;
            for (int i = 1; i < rm.Count; i++)
            {
                t = rm[i].RebuilderDelta();
                double t_avg = t.Item1;
                double t_st = t.Item2;
                if (t_avg < best_avg && t_st < best_st)
                {
                    num = i;
                    best_avg = t_avg;
                    best_st = t_st;
                }
            }
            return num;
        }
        static void WriteModelInfo(RegressModel m)
        {

            Console.WriteLine(m.GetEquation());
            Console.WriteLine("Адекватность {0}  Нормальность ошибки {1}", m.IsModelAdequate(), m.HasErrorNormalDistribution());
            Console.WriteLine();
        }
        static void Main(string[] args)
        {
            int m = 17;
            // Корелляция между параметрами (не более)
            
            double corelX = 0.7;
            // Корелляция параметра и y (не менее)
            double corelY = 0.03;
            double alp = 0.05;
            Console.WriteLine("Корелляция между параметрами (не более)");
            //corelX = double.Parse(Console.ReadLine());
            Console.WriteLine("Корелляция параметра и y (не менее)");
            //corelY = double.Parse(Console.ReadLine());
            Console.WriteLine("Альфа");
            //alp = double.Parse(Console.ReadLine());
            int variant = 1;//int.Parse(Console.ReadLine());
            StreamWriter sw = new StreamWriter("info.txt");
            Console.SetOut(sw);
            
            ExcelDataSource source = new ExcelDataSource(@"source.xls");
            source.ExtractData(m, variant, out List<Vector<double>> par, out Vector<double> Y);

            ModelGenerator generator = new ModelGenerator(corelX, corelY, alp);
            List<RegressModel> models = generator.GenerateModels(par, Y);

            for (int i = 0; i < models.Count; i++)
            {
                Console.WriteLine($"Model {i}");
                WriteModelInfo(models[i]);
            }


            var best1 = BestModelDet(models);
            var best2 = BestModelVar(models);
            var best3 = BestModelVarYear(models);

            Console.WriteLine($"Лучшая по детерминации {best1}");
            WriteModelInfo(models[best1]);
            Console.WriteLine($"Лучшая по вариациции коэффициентов {best2}");
            WriteModelInfo(models[best2]);
            Console.WriteLine($"Лучшая по вариациции коэффициентов (по годам) {best3}");
            WriteModelInfo(models[best3]);
            sw.Close();
        }
    }
}
