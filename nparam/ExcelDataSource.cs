﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Collections.Generic;
using System.IO;

namespace nparam
{
    class ExcelDataSource
    {
        IWorkbook wb;
        ISheet ws;
        readonly FileStream fs;
        public ExcelDataSource(string file)
        {
            fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            wb = new HSSFWorkbook(fs);
            ws = wb.GetSheetAt(0);
        }
        public Vector<double> GetCol(int row, int start, int count)
        {
            var v = DenseVector.Create(count, 0);
            for (int i = 0; i < count; i++)
            {
                v[i] = ws.GetRow(i + start).GetCell(row, MissingCellPolicy.RETURN_NULL_AND_BLANK).NumericCellValue;
            }
            return v;
        }
        public void ExtractData(int m,int variant, out List<Vector<double>> X, out Vector<double> Y)
        {
            X = new List<Vector<double>>();
            for (int i = 0; i < m; i++)
            {
                X.Add(GetCol(i + 2, 1, 770));
            }
            Y = GetCol(m + 1 + variant, 1, 770);
        }
    }
}
